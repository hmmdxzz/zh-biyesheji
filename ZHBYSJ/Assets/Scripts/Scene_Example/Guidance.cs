﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guidance : MonoBehaviour {

	public VirtualPlayer virtualPlayer;
	public UIManager uiManager;
	public Object_Bucket bucket;
	public Object_Tank tank;
	private int Level;

	public GameObject kongqiqiang1;
	public GameObject kongqiqiang2;
	public GameObject bridge1;
	public GameObject bridge2;

	public AudioSource audioSource;
	public AudioClip[] audioClip=new AudioClip[16];

	//判定标志位
	private bool level4 = false;
	private bool level10 = false;
	private bool level11 = false;
	private bool level13 = false;

	void Start () {
		Level = 0;
		virtualPlayer.transform.position = new Vector3(-10, 0.85f, -10);
        virtualPlayer.moveSpeed = 0;
        virtualPlayer.sensitivityHor = 0;
        virtualPlayer.sensitivityVer = 0;
        virtualPlayer.sensitivityTrigger = 0;

		kongqiqiang1.SetActive(true);
		kongqiqiang2.SetActive(true);
		bridge1.SetActive(false);
		bridge2.SetActive(false);

		audioSource = GetComponent<AudioSource>();
		uiManager.ClearTextTips();
		Invoke("LevelUp", 3);
		Debug.Log("教学开始");
		//audioSource.clip = audioClip[0];
		//audioSource.Play();
		//AudioSource.PlayClipAtPoint(audioClip[0], virtualPlayer.transform.position);
	}

	void Update()
    {
        if (!level4)
        {
            if (virtualPlayer.transform.position.x >= 6)
            {
				level4 = true;
				Level = 4;
				LevelUp();
            }
        }
		if(level4&&!level10)
            if (virtualPlayer.transform.position.z >= 5)
            {
				level10 = true;
				Level = 10;
				LevelUp();
            }
        if (level4 && level10 && !level11)
        {
			//Debug.Log(bucket.GetListCount());
            if (bucket.GetListCount() >= 3)
            {
				level11 = true;
				Level = 11;
				LevelUp();
            }
        }
		if(level4 && level10 && level11 && !level13)
        {
            if (tank.GetListCount() >= 1)
            {
				level13 = true;
				Level = 13;
				LevelUp();
            }
        }
    }

	private void LevelUp()
    {
		audioSource.clip = audioClip[Level];
		audioSource.Play();
		switch (Level)
        {
			case 0:
				uiManager.SetTextTips(TxtByLevel(Level++));
				Invoke("LevelUp", 6);
				break;
			case 1:
				uiManager.SetTextTips(TxtByLevel(Level++));
				//恢复转动能力
				virtualPlayer.sensitivityHor = 2;
				virtualPlayer.sensitivityVer = 2;
				Invoke("LevelUp", 6);
				break;
			case 2:
				uiManager.SetTextTips(TxtByLevel(Level++));
				virtualPlayer.moveSpeed = 2;
				Invoke("LevelUp", 8);
				break;
			case 3:
				uiManager.SetTextTips(TxtByLevel(Level++));//现在请移动到红色平台
				kongqiqiang1.SetActive(false);
				bridge1.SetActive(true);
				break;
			case 4:
				uiManager.SetTextTips(TxtByLevel(Level++));//移动已完成
				kongqiqiang1.SetActive(true);
				bridge1.SetActive(false);
				Invoke("LevelUp", 6);
				break;
			case 5:
				uiManager.SetTextTips(TxtByLevel(Level++));
				virtualPlayer.sensitivityTrigger = 10;
				Invoke("LevelUp", 6);
				break;
			case 6:
				uiManager.SetTextTips(TxtByLevel(Level++));
				Invoke("LevelUp", 8);
				break;
			case 7:
				uiManager.SetTextTips(TxtByLevel(Level++));
				Invoke("LevelUp", 6);
				break;
			case 8:
				uiManager.SetTextTips(TxtByLevel(Level++));
				Invoke("LevelUp", 6);
				break;
			case 9:
				uiManager.SetTextTips(TxtByLevel(Level++));
				kongqiqiang2.SetActive(false);
				bridge2.SetActive(true);
				break;
			case 10:
				uiManager.SetTextTips(TxtByLevel(Level++));
				break;
			case 11:
				uiManager.SetTextTips(TxtByLevel(Level++));//传送到黑色平台
				Invoke("LevelUp", 5);
				break;
			case 12:
				uiManager.SetTextTips(TxtByLevel(Level++));
				virtualPlayer.transform.position = new Vector3(-10, 0.85f, 7);
				virtualPlayer.transform.eulerAngles = new Vector3(0, 0, 0);
				bucket.transform.position = new Vector3(-10,0,10);
				break;
			case 13:
				uiManager.SetTextTips(TxtByLevel(Level++));
				Invoke("LevelUp", 6);
				break;
			case 14:
				uiManager.SetTextTips(TxtByLevel(Level++));
				Invoke("LevelUp", 6);
				break;
			case 15:
				uiManager.SetTextTips (TxtByLevel (Level));
				Invoke ("LevelUp", 30);
				break;
			default:
				uiManager.SetTextTips(TxtByLevel(Level));
				break;
		}
	}

	public void SetLevel(int i)
    {
		Level = i;
    }

	string TxtByLevel(int Level)
    {
        switch (Level)
        {
			case 0:
				return "您好欢迎来到虚拟垃圾分类平台";
			case 1:
				return "请通过鼠标转动视角";
			case 2:
				return "WSAD分别可以向前后左右移动";
			case 3:
				return "现在请移动到红色平台";
			case 4:
				return "屏幕正中央的彩色小球用来模拟VR手柄";
			case 5:
				return "使用鼠标滑轮可以控制彩色小球与您的距离";
			case 6:
				return "当小球碰到物体时，如果可以交互，则物体会变色";
			case 7:
				return "使用鼠标左键可以开门或拿起物体";
			case 8:
				return "使用鼠标右键可以关门或放下物体";
			case 9:
				return "现在请将可以交互的牛奶盒带到绿色平台";
			case 10:
				return "现在请将3种垃圾都扔进垃圾桶";
			case 11:
				return "任务完成，即将传送到黑色平台";
			case 12:
				return "现在请将小垃圾桶扔进大垃圾桶里";
			case 13:
				return "现在可以查看您的结果了";
			case 14:
				return "错误的分类将会显示在屏幕上";
			case 15:
				return "现在您可以通过ESC键返回主选单";
			default :
				return "???";
        }
    }

}
