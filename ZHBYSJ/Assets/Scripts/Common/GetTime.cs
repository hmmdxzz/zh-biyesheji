﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetTime : MonoBehaviour {

	private Text time;
	private DateTime time_first;
	private DateTime time_second;

	// Use this for initialization
	void Start () {
		time = GetComponent<Text>();
		time.text = "00:00";
		time_first = DateTime.Now;
	}
	
	// Update is called once per frame
	void Update () {
		time_second = DateTime.Now;
		TimeSpan time_x = time_second - time_first;
		time.text = time_x.Minutes.ToString() + ":" + time_x.Seconds.ToString();
	}

	public string GetTheTime()
    {
		return time.text;
    }

}
