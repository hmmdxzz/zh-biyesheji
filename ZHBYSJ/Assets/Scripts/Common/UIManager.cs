﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;//Unity的UI命名空间，文本，按钮，图像等类在该命名空间中
using UnityEngine.SceneManagement;//Unity的场景管理命名空间

/// <summary>
/// UI管理类
/// </summary>
public class UIManager : MonoBehaviour {

	//场景切换相关
	AsyncOperation operation;
	//该对象可以用来表示切换场景这一操作
	//并且具有数值属性progress用来表示当前操作的进度，范围在0~0.9
	//0表示操作刚开始，0.9表示操作完成
	private float progress;//目标场景加载进度
	private float Curprogress;//显示的加载进度，可用于设置进度条
	public int ChangeSceneIndex;//目标场景ID
	private bool isLoadScene = false;//标志位用来查看是否触发场景切换操作
	public GameObject LoadPanel;//场景切换过场界面

	public Text Txt_Name;//物体名称
	public Text Txt_Content;//垃圾桶内容物
	public Text Txt_Distance;//模拟手柄距离，VR场景中不显示
	public Text Txt_Progress;//关卡进度
	public Text Txt_Tips;//提示，仅在教学中显示

	//Unity:仅在首次调用Update方法之前调用Start
	void Start () {
		LoadPanel.SetActive(false);//不激活该物体
		Cursor.visible = false;//场景激活后隐藏鼠标
		ClearTextName();
		ClearTextContent();
		ClearTextDistance();
		ClearTextTips();
	}
	
	//Unity:每一帧调用一次该方法
	void Update () {

        if (Input.GetKey(KeyCode.Escape))//检测到ESC键被按下
        {
            if (!isLoadScene)//判断没有触发切换场景的操作
            {
				Cursor.visible = true;//显示鼠标
				isLoadScene = true;//修改标志位
				LoadPanel.SetActive(true);//显示加载场景过场图
				StartCoroutine(ChangeScene());//调用该线程异步加载场景
            }
        }
        if (operation == null)//该变量为空表示没有触发
        {
			return;
        }
		//operation.progess表示操作的执行进度，在此表示目标场景加载进度,该值等于0.9表示操作完成
		if (operation.progress < 0.899)
        {
			progress = operation.progress * 100;//获取当前操作进度
        }
		//进入该else就表示operation.progress的值等于0.9，代表场景实际已经加载完成
		//一般来说，场景加载的速度往往非常快，电脑性能好的话，加载实际用时也就几十毫秒
		//实际的加载时间对人眼来说太快了，过场里如果有图片或文字信息，用户根本就看不见
		//所以在启动场景加载的操作之后，要将allowSceneActivation变量置为false，防止他自动切换
		//而且必须引入另一变量（本脚本中该目的的变量为Curprogress）来表示假的加载进度
		else
		{
			//operation.progress的最大值为0.9，但是加载通常来说都喜欢取整，比如0%~100%
			//所以加载完成的话将实际加载进度取整
			progress = 100;
        }
		//比较假的加载进度和实际加载进度
		//如果假的加载进度比实际的加载进度数值小
		//那假的进度就每帧增加一点点，本脚本选择每帧数值加1
		//电脑性能对帧数影响较大，该操作实际显示加载时间在大概1-3秒之间
        if (Curprogress < progress)
        {
			Curprogress += 1;
        }
        if (Curprogress >= 100)//假的加载进度达到100
        {
			//allowSceneActivation表示是否在场景加载完毕后自动激活
			operation.allowSceneActivation = true;//场景自动跳转
        }
	}

	IEnumerator ChangeScene()
    {
		//SceneManager.LoadSceneAsync方法用于异步加载场景
		//第一个参数为要加载的场景的ID
		//第二个参数为枚举变量，用于表示加载场景的方式,LoadSceneMode.Single表示激活目标场景后销毁其他已激活场景，LoadSceneMode.Additive表示激活目标场景后不对已激活场景做操作
		operation = SceneManager.LoadSceneAsync(ChangeSceneIndex,LoadSceneMode.Single);
		operation.allowSceneActivation = false;//不允许场景加载完毕自动激活
        
		//operation.isDone表示操作是否完成
		while (!operation.isDone&&operation.progress<1)
        {
			yield return operation;
        }
    }

	//设置UI文本内容，Text类型表示Unity中的Text组件，组件中的string类型属性text才是真正的文本内容
	public void SetTextName(string name)
    {
		Txt_Name.text = name;
    }
	public void SetTextContent(string content)
    {
		Txt_Content.text = content;
    }
	public void SetTextDistance(string distance)
    {
		Txt_Distance.text = distance;
    }
	public void SetTextProgress(string progress)
    {
		Txt_Progress.text = progress;
    }
	public void SetTextTips(string tip)
    {
		Txt_Tips.text = tip;
    }
	public void ClearTextName()
    {
		Txt_Name.text = "";
    }
	public void ClearTextContent()
    {
		Txt_Content.text = "";
    }
	public void ClearTextDistance()
    {
		Txt_Distance.text = "";
    }
	public void ClearTextProgress()
    {
		Txt_Progress.text = "";
    }
	public void ClearTextTips()
    {
		Txt_Tips.text = "";
    }
}
