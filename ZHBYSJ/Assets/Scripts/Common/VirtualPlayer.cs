﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 键盘鼠标操作的虚拟玩家，在场景中使用一个胶囊碰撞器代替玩家人体，摄像机模拟玩家眼睛作为玩家物体的子物体，小球模拟虚拟手柄作为摄像机的子物体
/// </summary>
public class VirtualPlayer : MonoBehaviour {

    //移动相关
    public float moveSpeed = 2f;
    private Transform transform;//本物体的transform组件

    //视角转动
    public float sensitivityHor = 2f;//鼠标水平灵敏度
    public float sensitivityVer = 2f;//鼠标竖直灵敏度
    private float downVer = -70;//摄像机俯角限制
    private float upVer = 80;//摄像机仰角限制
    private float rotVer;//最终计算结果
    private float rotHor;//最终计算结果

    //手柄移动相关
    public GameObject controller;//模拟过程中小球的索引
    //使用摄像机前方的小球表示VR手柄，使用小球距离摄像机的距离表示人手臂伸展去触碰物体
    public float sensitivityTrigger=50f;//鼠标滑轮的灵敏度
    public float upTrigger = 2.0f;//小球的距离上限
    public float downTrigger = 0.5f;//小球的距离下限
    private float distance;//最终计算结果

    public GameObject mainCamera;//主摄像机
    public UIManager uiManager;//获取UIManager的引用以便设置TxtDistance

    //Unity:仅在首次调用Update方法之前调用Start
    void Start()
    {
        transform = this.GetComponent<Transform>();//获取玩家的transform组件
        rotVer = mainCamera.transform.eulerAngles.x;//transform.eulerAngles(欧拉角)表示的就是transform组件中的rotation，也就是物体朝向
    }

    //Unity:每一帧调用一次该方法
    void Update()
    {
        //视角转动
        float mouseVer = Input.GetAxis("Mouse Y");//获取鼠标在Y轴上的位移量
        float mouseHor = Input.GetAxis("Mouse X");//获取鼠标在X轴上的位移量
        rotVer -= mouseVer * sensitivityVer;//当前俯仰角减去(纵向位移量*纵向灵敏度)
        rotVer = Mathf.Clamp(rotVer, downVer, upVer);//俯仰角限制
        rotHor = transform.eulerAngles.y + mouseHor * sensitivityHor;//水平朝向角+横向位移量*横向灵敏度
        //transform组件中的指都是以3维向量的形式存储，所以赋值时使用Vector3类型
        mainCamera.transform.localEulerAngles = new Vector3(rotVer, 0, 0);//设置摄像机俯仰角
        transform.eulerAngles = new Vector3(0, rotHor, 0);//设置玩家的水平朝向

        //角色移动
        //KeyCode是UnityEngine中定义的键盘的枚举变量，写法举例
        //KeyCode.Y(字母键Y)    KeyCode.P(字母键P)    KeyCode.Alpha0(数字键0)    KeyCode.Alpha5(数字键5)
        //KeyCode.UpArrow(方向键上)    KeyCode.LeftArrow(方向键左)    KeyCode.LeftShift(左Shift)    KeyCode.Escape(ESC键)
        //Input.GetKey(KeyCode Key)方法会在传入的按键被按下的时候返回值为true
        if (Input.GetKey(KeyCode.W))//前
            //Translate(Vector3 translation)方法会将传入的三维向量叠加到当前物体所处的坐标上，从而实现移动
            //Vector3.forward由Unity提供，为与当前物体朝向相同的单位向量，同理Vector3.back为与当前物体朝向相反的单位向量
            //Time.deltaTime由Unity提供，为两帧之间的时间间隔，因各种原因会导致相邻帧之间间隔时间不固定，所以要用速度乘以该时间间隔使物体匀速移动
            transform.Translate(Vector3.forward * Time.deltaTime * moveSpeed);
        if (Input.GetKey(KeyCode.S))//后
            transform.Translate(Vector3.back * Time.deltaTime * moveSpeed);
        if (Input.GetKey(KeyCode.A))//左
            transform.Translate(Vector3.left * Time.deltaTime * moveSpeed);
        if (Input.GetKey(KeyCode.D))//右
            transform.Translate(Vector3.right * Time.deltaTime * moveSpeed);

        //手柄距离
        float mouseSrc = Input.GetAxis("Mouse ScrollWheel");//获取鼠标滑轮滚动量
        //localPosition为物体在父级物体坐标轴下的坐标，小球相对于摄像机只能前后移动，所以只需要设置localPosition.z
        float mouseScrollWheel = controller.transform.localPosition.z;
        distance = mouseScrollWheel + mouseSrc * Time.deltaTime * sensitivityTrigger;
        distance = Mathf.Clamp(distance, downTrigger, upTrigger);//限制距离上下限
        controller.transform.localPosition = new Vector3(0, 0, distance);
        //string.Format()方法用来规范化字符串，此处将distance格式化为小数点后两位
        uiManager.SetTextDistance("模拟手柄距离：" + string.Format("{0:N}", distance) + "m");
    }
}
