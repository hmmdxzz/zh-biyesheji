﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 垃圾桶类
/// </summary>
public class Object_Bucket : MonoBehaviour {

	[SerializeField]
	private string name;//垃圾桶名字
	private List<RubbishItem> RubbishList;//垃圾列表
	private string content;//垃圾桶内容物
	private string objectTag;//垃圾标签
	[SerializeField]
	private UIManager uiManager;
	public GameObject particleSystem;//触发碰撞后显示的特效

	//两个变量用来存储管卡开始时可能存在的垃圾
	[SerializeField]
	private string rubbishname;
	[SerializeField]
	private RubbishGroup rubbishgroup;

	//Unity:仅在首次调用Update方法之前调用Start
	void Start () {
		objectTag = "Object_Rubbish";//指定碰撞物标签
		RubbishList = new List<RubbishItem>();//创建列表

		//如果可能存在的垃圾名字不为空则将该项加入内容物列表
        if (rubbishname != "") 
		{
			RubbishItem rubbishItem;
			rubbishItem.name = rubbishname;
			rubbishItem.group = rubbishgroup;
			RubbishList.Add(rubbishItem);
		}
		//Debug.Log(RubbishList.Count);
	}

	public string GetName()//获取名字
    {
		return name;
    }
	
	public string GetContent()//获取内容物
    {
		content = "<b>内容物：</b>";//初始化
		if (RubbishList.Count <= 0)//若列表为空则返回空
			return content + "\n空";
		foreach (RubbishItem element in RubbishList)//列表不为空，遍历列表添加名字
        {
			content += "\n" + element.name;
        }
		return content;
    }

	//获取当前内容物列表，会在碰撞大垃圾桶的时候被调用
	public List<RubbishItem> GetRubbishList()
    {
		return RubbishList;
    }

	//获取当前内容物数量，在教程中被调用，作为判断是否进入下一步骤的条件
	public int GetListCount()
    {
		return RubbishList.Count;
    }

	//Unity:当此碰撞器/刚体开始接触另一个刚体/碰撞器时，调用OnCollisionEnter
	private void OnCollisionEnter(Collision collision)
    {
		if(collision.gameObject.tag==objectTag)//通过标签判断该物体是不是垃圾
        {
			Object_Rubbish _Rubbish = collision.gameObject.GetComponent<Object_Rubbish>();//获取该垃圾的Object_Rubbish脚本组件
			RubbishItem rubbishItem;//创建垃圾结构体局部变量
			rubbishItem.name = _Rubbish.GetName();//获取该垃圾的名字
			rubbishItem.group = _Rubbish.GetGroup();//获取该垃圾的种类
			RubbishList.Add(rubbishItem);//将该结构体加入列表
			Destroy(collision.gameObject);//销毁垃圾物体
			Instantiate(particleSystem, transform);//产生一个特效作为提醒

			//如果垃圾桶的父级不为空表明本垃圾桶已经被拿起，碰到垃圾后调用UIManager更新Content
			if (transform.parent != null) 
				uiManager.SetTextContent(GetContent());
        }
    }
}