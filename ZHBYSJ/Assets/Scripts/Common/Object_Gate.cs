﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object_Gate : MonoBehaviour {

	[SerializeField]
	private string name;
	[SerializeField]
	private string content;
	public Vector3 targetPositon;
	public Vector3 targetRotation;
	[SerializeField]
	private bool IsVRActive=false;
	private bool IsPlayerOut=false;
	public GameObject player;
	public GameObject[] buckets=new GameObject[4];
	public AudioClip audio_in;
	public AudioClip audio_out;
	private AudioSource audioSource;

	void Start()
    {
		audioSource = GetComponent<AudioSource>();
		audioSource.playOnAwake = false;
		audioSource.clip = audio_in;
		InvokeRepeating("PlayTheAudio", 5, 60);
    }

	public string GetName()
    {
		return name;
    }
	public string GetContent()
    {
		return content;
    }
	public void OpenTheDoor()
    {
		IsPlayerOut = true;
		if (!IsVRActive)
			player.transform.eulerAngles = targetRotation;
		player.transform.position = targetPositon;
		float temp = -1.5f;
		foreach (GameObject element in buckets)
			element.transform.position = new Vector3(0, 4, temp+=0.6f);
		CancelInvoke("PlayTheAudio");
		audioSource.clip = audio_out;
		InvokeRepeating("PlayTheAudio", 5, 60);
    }

	void PlayTheAudio()
    {
		audioSource.Play();
    }
}
