﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 垃圾类
/// </summary>
public class Object_Rubbish : MonoBehaviour {

	//垃圾名字
	[SerializeField]
	private string name;
	//垃圾种类
	[SerializeField]
	private RubbishGroup group;

	public string GetName()//获取名字
    {
		return name;
    }
    public RubbishGroup GetGroup()//获取种类
    {
		return group;
    }
}
