﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class InteractObjectVREvent : MonoBehaviour {

	public UIManager uiManager;
	VRTK_InteractableObject _interactableEvent;

	private string rubbishTag = "Object_Rubbish";
	private string bucketTag = "Object_Bucket";
	private string doorTag = "Object_Door";
	private string gateTag = "Object_Gate";

	void Start(){
		_interactableEvent = GetComponent<VRTK_InteractableObject> ();
		_interactableEvent.InteractableObjectTouched += onObjectTouched;
		_interactableEvent.InteractableObjectUntouched += onObjectUntouched;
		_interactableEvent.InteractableObjectUsed += onObjectUsed;
	}

	private void onObjectTouched(object sender,InteractableObjectEventArgs e)
    {
		if (tag == rubbishTag)
        {
			uiManager.SetTextName(GetComponent<Object_Rubbish>().GetName());
        }
		else if (tag == bucketTag)
        {
			uiManager.SetTextName(GetComponent<Object_Bucket>().GetName());
			uiManager.SetTextContent(GetComponent<Object_Bucket>().GetContent());
		}
		else if (this.gameObject.tag == doorTag)
        {
			uiManager.SetTextName(GetComponent<Object_Door>().GetName());
        }
		else if (this.gameObject.tag == gateTag)
        {
			uiManager.SetTextName(GetComponent<Object_Gate>().GetName());
			uiManager.SetTextContent(GetComponent<Object_Gate>().GetContent());
        }
    }

	private void onObjectUntouched(object sender,InteractableObjectEventArgs e)
    {
		uiManager.ClearTextName();
		uiManager.ClearTextContent();
    }

	private void onObjectUsed(object sender,InteractableObjectEventArgs e)
    {
		if (tag == doorTag)
        {
			Object_Door door=GetComponent<Object_Door>();
			if (!door.GetIsOpen())
				door.OpenTheDoor();
			else
				door.CloseTheDoor();
        }
		else if (tag == gateTag)
        {
			Object_Gate gate = GetComponent<Object_Gate>();
			gate.OpenTheDoor();
        }
    }
}
