﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleAutoDestruction : MonoBehaviour {

	[SerializeField]
	private int time;
	void Start () {
		Invoke("DestoryParticle", time);
	}

	void DestoryParticle()
    {
		Destroy(this.gameObject);
    }

}
