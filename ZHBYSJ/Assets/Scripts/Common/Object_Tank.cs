﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object_Tank : MonoBehaviour {

	public ScoreManager scoreManager;
	[SerializeField]
	private RubbishGroup group;

	public AudioClip rightaudio;
	public AudioClip wrongaudio;
	public GameObject particleSystem;

	private string RubbishTag = "Object_Rubbish";
	private string BucketTag="Object_Bucket";
	private bool IsAllRight;
	private List<RubbishItem> rubbishItems=new List<RubbishItem>();

	void OnCollisionEnter(Collision collision)
    {
		if (collision.gameObject.tag == RubbishTag)
        {
			RubbishItem rubbishItem;
			Object_Rubbish rubbish = collision.gameObject.GetComponent<Object_Rubbish>();
			rubbishItem.name = rubbish.GetName();
			rubbishItem.group = rubbish.GetGroup();
			Destroy(collision.gameObject);
			rubbishItems.Add(rubbishItem);
			IsAllRight = scoreManager.GetTheResult(rubbishItem, group);
			PlayTheAudio(IsAllRight);
        }
		else if (collision.gameObject.tag == BucketTag)
        {
			List<RubbishItem> rubbishList = collision.gameObject.GetComponent<Object_Bucket>().GetRubbishList();
			Destroy(collision.gameObject);
			foreach (RubbishItem element in rubbishList)
				rubbishItems.Add(element);
			IsAllRight = scoreManager.GetTheResults(rubbishList, group);
			PlayTheAudio(IsAllRight);
		}
	}
	
	void PlayTheAudio(bool right)
    {
        if (right)
        {
			AudioSource.PlayClipAtPoint(rightaudio, transform.position);
			Instantiate(particleSystem, transform);
        }
        else
        {
			AudioSource.PlayClipAtPoint(wrongaudio, transform.position);
        }
    }

	public int GetListCount()
    {
		return rubbishItems.Count;
    }
}
