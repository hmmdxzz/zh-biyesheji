﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object_Door : MonoBehaviour {

	public string name;
	private bool IsOpen;
	[SerializeField]
	private float tranX, tranZ, rotaZ;//0.42,0.38 90//
	private AudioSource audioSource;

	void Start () {
		IsOpen = false;
		audioSource = GetComponent<AudioSource>();
		audioSource.playOnAwake = false;
	}

	public string GetName()
    {
		return name;
    }
	public bool GetIsOpen()
    {
		return IsOpen;
    }

	public void OpenTheDoor()
    {
        if (!IsOpen)
        {
			audioSource.Play();
			this.transform.Translate(new Vector3(tranX, 0, tranZ), Space.World);
			this.transform.Rotate(new Vector3(0, 0, rotaZ), Space.Self);
			IsOpen = !IsOpen;
        }
    }
	public void CloseTheDoor()
    {
        if (IsOpen)
        {
			audioSource.Play();
			this.transform.Translate(new Vector3(-tranX, 0, -tranZ), Space.World);
			this.transform.Rotate(new Vector3(0, 0, -rotaZ), Space.Self);
			IsOpen = !IsOpen;
        }
    }

	public void SwitchTheDoor()
    {
		if (IsOpen)
			CloseTheDoor();
		else
			OpenTheDoor();
    }
}
