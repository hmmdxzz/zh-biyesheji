﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

	public GetTime getTime;
	public Text Txt_Result;//结果
	public Text[] Txt_Details;

	private int combo = 1;
	private int right = 0;
	private int wrong = 0;
	private int score = 0;
	public int maxnum = 10;//详情每组最大数量

	// Use this for initialization
	void Start () {
		ClearTextResult();
		ClearTextDetails();
	}

	public bool GetTheResult(RubbishItem rubbishItem,RubbishGroup group)
    {
		Debug.Log("开始进行比较");
		bool IsRight = true;
		if (rubbishItem.group == group)
        {
			right++;
			score += combo * 10;
			combo++;
			combo = (int)Mathf.Clamp(combo, 1, 3);
        }
        else
        {
			IsRight = false;
			wrong++;
			score -= 5;
			combo = 1;
			SetTextDetails(GetColor(rubbishItem.group) + rubbishItem.name + "</color>  属于  "
				+ GetColor(rubbishItem.group) + Chinese(rubbishItem.group) + "</color>");
        }
		SetTextResult("正确率：" + right + "/" + (right + wrong) + "        分数：" + score + "        时间:" + getTime.GetTheTime());
		return IsRight;
    }
	public bool GetTheResults(List<RubbishItem> rubbishList,RubbishGroup group)
    {
		bool IsAllRight = true;
		foreach(RubbishItem rubbishItem in rubbishList)
        {
			if (GetTheResult(rubbishItem, group) == false)
				IsAllRight = false;
        }
		return IsAllRight;
    }

	string Chinese(RubbishGroup group)
	{
		switch (group)
		{
			case RubbishGroup.KEHUISHOUWU:
				return "可回收物";
			case RubbishGroup.YIFULAJI:
				return "易腐垃圾";
			case RubbishGroup.YOUHAILAJI:
				return "有害垃圾";
			case RubbishGroup.QITALAJI:
				return "其他垃圾";
			default:
				return "你有问题";
		}
	}
	string GetColor(RubbishGroup group)
	{
		switch (group)
		{
			case RubbishGroup.KEHUISHOUWU:
				return "<color=#0000FF>";
			case RubbishGroup.YIFULAJI:
				return "<color=#00FF00>";
			case RubbishGroup.YOUHAILAJI:
				return "<color=#FF0000>";
			case RubbishGroup.QITALAJI:
				return "<color=#000000>";
			default:
				return "我觉得你有问题";

		}
	}

	void SetTextResult(string result)
    {
		Txt_Result.text = result;
    }
	void SetTextDetails(string detail)
    {
		Txt_Details[wrong / maxnum].text += detail + "\n";
    }
	void ClearTextResult()
    {
		Txt_Result.text = "";
    }
	void ClearTextDetails()
    {
		foreach(Text detail in Txt_Details)
        {
			detail.text = "";
        }
    }
}
