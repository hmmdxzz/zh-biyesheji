﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractWithObject : MonoBehaviour {

	private string rubbishTag = "Object_Rubbish";
    private string bucketTag = "Object_Bucket";
    private string doorTag = "Object_Door";
    private string gateTag = "Object_Gate";

	private GameObject currentObject;
    public Color highlightColor = Color.red;
    public bool precisionPick = true;

    public UIManager uiManager;
	
	void FixedUpdate () {
        if (Input.GetMouseButton(0))
        {
            pickUpObject();
        }
        if (Input.GetMouseButton(1))
        {
            dropObject();
        }
	}

	private void pickUpObject()
    {
        if (currentObject != null&&GetComponentsInChildren<Transform>(true).Length<=1)//不为空且目前没有子物体
        {
            if(currentObject.gameObject.tag==rubbishTag)
            {
                currentObject.transform.parent = transform;
                Object_Rubbish rubbish = currentObject.GetComponent<Object_Rubbish>();
                uiManager.SetTextName(rubbish.GetName());
                SetObjectColor(currentObject.gameObject,Color.white);
                Rigidbody rigidbody = currentObject.GetComponent<Rigidbody>();
                rigidbody.useGravity = false;
                rigidbody.isKinematic = true;
                if (precisionPick)
                    currentObject.transform.localPosition = Vector3.zero;
            }
            else if (currentObject.gameObject.tag == bucketTag)
            {
                currentObject.transform.parent = transform;
                Object_Bucket bucket = currentObject.GetComponent<Object_Bucket>();
                uiManager.SetTextName(bucket.GetName());
                uiManager.SetTextContent(bucket.GetContent());
                SetObjectColor(currentObject.gameObject, Color.white);
                Rigidbody rigidbody = currentObject.GetComponent<Rigidbody>();
                rigidbody.useGravity = false;
                rigidbody.isKinematic = true;
                if (precisionPick)
                    currentObject.transform.localPosition = Vector3.zero;
            }
            else if (currentObject.gameObject.tag == doorTag)
            {
                Object_Door door = currentObject.GetComponent<Object_Door>();
                SetObjectColor(currentObject.gameObject, Color.white);
                uiManager.SetTextName(door.GetName());
                door.OpenTheDoor();
            }
            else if (currentObject.gameObject.tag == gateTag)
            {
                Object_Gate gate = currentObject.GetComponent<Object_Gate>();
                SetObjectColor(currentObject.gameObject, Color.white);
                uiManager.SetTextName(gate.GetName());
                uiManager.SetTextContent(gate.GetContent());
                gate.OpenTheDoor();
            }
        }
    }

	private void dropObject()
    {
        if (GetComponentsInChildren<Transform>(true).Length>1)//放下物体时要有子物体
        {
            currentObject = GetComponentsInChildren<Transform>(true)[1].gameObject;
            currentObject.transform.parent = null;
            currentObject.transform.eulerAngles = new Vector3(-90,0,0);
            Rigidbody rigidbody = currentObject.GetComponent<Rigidbody>();
            rigidbody.useGravity = true;
            rigidbody.isKinematic = false;
        }
        else if (currentObject!=null&&currentObject.gameObject.tag == doorTag)//放不下物体时必须要是个门
        {
            currentObject.GetComponent<Object_Door>().CloseTheDoor();
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (GetComponentsInChildren<Transform>(true).Length > 1)//当前有子物体则不做反应
            return;

        if (collision.gameObject.tag == rubbishTag)
        {
            currentObject = collision.gameObject;
            SetObjectColor(currentObject, highlightColor);
            uiManager.SetTextName(currentObject.GetComponent<Object_Rubbish>().GetName());
        }
        else if (collision.gameObject.tag == bucketTag)
        {
            currentObject = collision.gameObject;
            SetObjectColor(currentObject, highlightColor);
            uiManager.SetTextName(currentObject.GetComponent<Object_Bucket>().GetName());
            uiManager.SetTextContent(currentObject.GetComponent<Object_Bucket>().GetContent());
        }
        else if (collision.gameObject.tag == doorTag)
        {
            currentObject = collision.gameObject;
            SetObjectColor(collision.gameObject, highlightColor);
            uiManager.SetTextName(currentObject.GetComponent<Object_Door>().GetName());
        }
        else if (collision.gameObject.tag == gateTag)
        {
            currentObject = collision.gameObject;
            SetObjectColor(collision.gameObject, highlightColor);
            uiManager.SetTextName(currentObject.GetComponent<Object_Gate>().GetName());
            uiManager.SetTextContent(currentObject.GetComponent<Object_Gate>().GetContent());
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        if (currentObject != null)
        {
            string tag = collision.gameObject.tag;
            if (tag == rubbishTag || tag == bucketTag || tag == doorTag || tag == gateTag) 
            {
                SetObjectColor(currentObject, Color.white);
                currentObject = null;
                uiManager.ClearTextName();
                uiManager.ClearTextContent();
            }
        }
    }

    private void SetObjectColor(GameObject gameObject,Color color)
    {
        Material[] materials = gameObject.GetComponent<MeshRenderer>().materials;
        foreach (Material element in materials)
        {
            element.color =new Color(color.r,color.g,color.b,element.color.a);
        }
    }
}
