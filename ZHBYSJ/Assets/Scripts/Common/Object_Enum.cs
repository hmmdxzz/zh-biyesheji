﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//垃圾种类枚举变量
public enum RubbishGroup
{
    KEHUISHOUWU,
    YIFULAJI,
    YOUHAILAJI,
    QITALAJI
}

//垃圾结构体，需要存储名字和种类
public struct RubbishItem
{
    public string name;
    public RubbishGroup group;
}