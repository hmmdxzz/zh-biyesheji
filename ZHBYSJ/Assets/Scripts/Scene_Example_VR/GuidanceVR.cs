﻿/*17281232 朱昊
 * 北京交通大学
 * 计算机与信息技术学院
 * 计科1704班
 * 指导教师：王冰*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuidanceVR : MonoBehaviour {

	public GameObject player;
	public UIManager uiManager;
	public Object_Bucket bucket;
	public Object_Tank tank;
	private int Level;

	public GameObject kongqiqiang1;
	public GameObject kongqiqiang2;
	public GameObject bridge1;
	public GameObject bridge2;

	public AudioSource audioSource;
	public AudioClip[] audioClip = new AudioClip[14];

	//判定标志位
	private bool level4 = false;
	private bool level8 = false;
	private bool level9 = false;
	private bool level11 = false;

	void Start()
	{
		Level = 0;
		player.transform.position = new Vector3(-10, 0f, -10);

		kongqiqiang1.SetActive(true);
		kongqiqiang2.SetActive(true);
		bridge1.SetActive(false);
		bridge2.SetActive(false);

		audioSource = GetComponent<AudioSource>();
		uiManager.ClearTextTips();
		Invoke("LevelUp", 3);
		Debug.Log("教学开始");
	}

	void Update()
	{
		if (!level4)
		{
			if (player.transform.position.x >= 5)
			{
				level4 = true;
				Level = 4;
				LevelUp();
			}
		}
		if (level4 && !level8)
			if (player.transform.position.z >= 5)
			{
				level8 = true;
				Level = 8;
				LevelUp();
			}
		if (level4 && level8 && !level9)
		{
			if (bucket.GetListCount() >= 3)
			{
				level9 = true;
				Level = 9;
				LevelUp();
			}
		}
		if (level4 && level8 && level9 && !level11)
		{
			if (tank.GetListCount() >= 1)
			{
				level11 = true;
				Level = 11;
				LevelUp();
			}
		}
	}

	private void LevelUp()
	{
		audioSource.clip = audioClip[Level];
		audioSource.Play();
		if (Level == 0)
		{
			uiManager.SetTextTips(TxtByLevel(Level++));
			Invoke("LevelUp", 6);
		}
		else if (Level == 1)
		{
			uiManager.SetTextTips(TxtByLevel(Level++));
			Invoke("LevelUp", 6);
		}
		else if (Level == 2)
		{
			uiManager.SetTextTips(TxtByLevel(Level++));
			Invoke("LevelUp", 6);
		}
		else if (Level == 3)
		{
			uiManager.SetTextTips(TxtByLevel(Level++));//移动到红色平台
			kongqiqiang1.SetActive(false);
			bridge1.SetActive(true);
		}
		else if (Level == 4)
		{
			uiManager.SetTextTips(TxtByLevel(Level++));//移动已完成
			kongqiqiang1.SetActive(true);
			bridge1.SetActive(false);
			Invoke("LevelUp", 6);
		}
		else if (Level == 5)
		{
			uiManager.SetTextTips(TxtByLevel(Level++));
			Invoke("LevelUp", 6);
		}
		else if (Level == 6)
		{
			uiManager.SetTextTips(TxtByLevel(Level++));
			Invoke("LevelUp", 6);
		}
		else if (Level == 7)
		{
			uiManager.SetTextTips(TxtByLevel(Level++));//移动到绿色平台
			kongqiqiang2.SetActive(false);
			bridge2.SetActive(true);
		}
		else if (Level == 8)
		{
			uiManager.SetTextTips(TxtByLevel(Level++));//将三种垃圾扔进
			kongqiqiang2.SetActive(false);
			bridge2.SetActive(true);
		}
		else if (Level == 9)
		{
			uiManager.SetTextTips(TxtByLevel(Level++));
			Invoke("LevelUp", 6);
		}
		else if (Level == 10)
		{
			uiManager.SetTextTips(TxtByLevel(Level++));
			player.transform.position = new Vector3(-10, 0f, 7);//传送到黑色平台
			//player.transform.eulerAngles = new Vector3(0, 0, 0);
			bucket.transform.position = new Vector3(-10, 0, 10);
		}
		else if (Level == 11)
		{
			uiManager.SetTextTips(TxtByLevel(Level++));
			Invoke("LevelUp", 6);
		}
		else if (Level == 12)
		{
			uiManager.SetTextTips(TxtByLevel(Level++));
			Invoke("LevelUp", 6);
		}
		else if (Level == 13)
		{
			uiManager.SetTextTips(TxtByLevel(Level));
			Invoke("LevelUp", 30);
		}
	}

	public void SetLevel(int i)
	{
		Level = i;
	}

	string TxtByLevel(int Level)
	{
		switch (Level)
		{
			case 0:
				return "您好，欢迎来到VR垃圾分类平台";
			case 1:
				return "您可以通过VR头盔观察环境";
			case 2:
				return "使用手柄触摸板可以在场景中移动";
			case 3:
				return "现在请移动到红色平台";
			case 4:
				return "利用VR手柄可以与物体进行交互";
			case 5:
				return "手柄碰到可以交互的物体会高亮显示";
			case 6:
				return "此时按下手柄扳机键会触发功能";
			case 7:
				return "现在请将可以拾取的牛奶盒带到绿色平台";
			case 8:
				return "现在请将3种垃圾都扔进垃圾桶";
			case 9:
				return "任务完成，即将传送到黑色平台";
			case 10:
				return "现在请将小垃圾桶扔进大垃圾桶里";
			case 11:
				return "现在可以查看您的结果了";
			case 12:
				return "错误的分类将会显示在屏幕上";
			case 13:
				return "现在您可以通过ESC键返回主选单";
			default:
				return "？？？？？";
		}
	}

}
