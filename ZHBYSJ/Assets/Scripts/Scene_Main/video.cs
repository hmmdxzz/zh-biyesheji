﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class video : MonoBehaviour {

	AudioSource audio;
	MovieTexture movie;
	Button button;

	void Start()
    {
		audio = GetComponent<AudioSource>();
		movie = (MovieTexture)GetComponent<RawImage>().mainTexture;
		movie.loop = true;
		button = GetComponent<Button>();
		button.onClick.AddListener(play);
    }

	public void PlayFromBegin()//从头播放
	{
		if (movie == null)//时序有问题，点击一次获取不到mov
			return;
		movie.Stop();
		audio.Stop();
		movie.Play();
		audio.Play();
    }

	public void play()
	{
        if (!movie.isPlaying)
        {
			movie.Play();
			audio.Play();

        }
		else
        {
			movie.Pause();
			audio.Pause();
        }
	}

}
