﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour {

    public GameObject mainPanel;//主界面UI
    public GameObject startPanel;//文本界面UI
    public GameObject loadPanel;//过场图UI

    public Text Txt_Content;//文本内容
    public GameObject Img_Video;//视频纹理
    
    bool isLoadingScene = false;
    private int ChangeSceneIndex;//跳转场景索引
    AsyncOperation operation;//加载场景操作
    private float progress;//场景加载进度
    private float Curprogress;//实际加载进度
    public Slider slider;//过场进度条
    public Text Txt_Progress;//过场加载进度

    private string[] txt= {
        //垃圾分类现状
        "<color=#FFFFFF00>wdnm</color>随着我们的生活越来越便利、商品越来越丰富，我们丢弃的生活垃圾也变得越来越多。北京这个拥有2100万人口的特大城市，随着经济发展和人口增加，生活垃圾产生量不断上升。2009年，北京市生活垃圾产生量为669万吨，平均每日清运1.83万吨。2019年全市生活垃圾清运量1011.16万吨，日均2.77万吨。" +
            "\n<color=#FFFFFF00>wdnm</color>生活垃圾的处理方式主要有焚烧发电、生化处理、和卫生填埋三种。目前，生活垃圾处理设施共40座，总设计处理能力32711吨/日。全市生活垃圾处理能力可满足生活垃圾处理需求。与日俱增的垃圾产生量，给垃圾处理工作带来了巨大的压力和挑战。目前北京市生活垃圾卫生填埋场库容趋于饱和，垃圾处理能力“紧平衡，缺弹性”。面对“垃圾围城”的紧迫形式，开展垃圾分类、垃圾减量势在必行。",
        //垃圾分类意义
        "<color=#FFFFFF00>wdnm</color><b>1.改善群众生活环境</b>" +
            "\n<color=#FFFFFF00>wdnm</color>生活垃圾如果随意堆放或简单填埋，会污染土壤、水源和空气，还会孳生大量蚊蝇和细菌，传染疾病，直接危害人们的身体健康。生活垃圾恶臭物质主要来源于厨余垃圾中的有机物质，如果不进行分类，垃圾中的含水量会变高，恶臭物质的释放就会变多。混合垃圾在收集、运输、处理工程中容易发酵，产生渗沥液，污染环境。\n" +
            "<color=#FFFFFF00>wdnm</color><b>2.有利于资源循环</b>" +
            "\n<color=#FFFFFF00>wdnm</color>如果不分出厨余垃圾,可回收物就会被污染，只能被焚烧，填埋，这将会降低资源的利用比例。而通过垃圾分类把废弃物中有回收利用价值的纸类、塑料类、玻璃类、金属类、织物类以及电器类分离出来循环利用，就可以减少垃圾处理量。\n" +
            "<color=#FFFFFF00>wdnm</color><b>3.促进全民文明素质的提升</b>" +
            "\n<color=#FFFFFF00>wdnm</color>垃圾分类是一项需要全民参与的系统工程，垃圾分类把大家紧紧联系在一起。每一个人、每一个家庭都实行垃圾分类，就能共同缔造美好生活环境。这不仅是城市治理能力的体现，也是文明素质提升、文明习惯养成、“绿色意识”增加的重要体现。",
        //垃圾分类标准
        "<color=#FFFFFF00>wdnm</color>北京市生活垃圾分为厨余垃圾、可回收物、有害垃圾、和其他垃圾四类。厨余垃圾收集容器是绿色的，可回收物收集容器是蓝色的，有害垃圾收集容器是红色的，其他垃圾收集容器是灰色的。" +
            "\n<color=#FFFFFF00>wdnm</color>北京市生活垃圾处理工作按照“干湿分开，资源回收，末端处理前端”的原则开展。在生活垃圾中应首先分出厨余垃圾，这样可以避免其他的可回收物被污染，保持厨余垃圾的纯净度，能更有效地进行生化处理；分出可回收物能最大限度地回收资源、循环利用；分出有害垃圾可以杜绝环境污染和人体健康的伤害；分出其他垃圾能够提高焚烧效率。",
        //厨余垃圾
        "<color=#FFFFFF00>wdnm</color><b>厨余垃圾：</b>" +
            "\n<color=#FFFFFF00>wdnm</color>是指家庭中产生的菜帮菜叶、瓜果皮核、剩饭剩菜、废弃食物等易腐性垃圾；从事餐饮经营活动的企业和机关、部队、学校、企业事业等单位集体食堂在食品加工、餐饮服务、单位供餐等活动中产生的食物残渣、食品加工肥料和废弃食用油脂；以及农贸市场、农产品批发市场产生的蔬菜瓜果垃圾、腐肉、肉碎骨、水产品、畜禽内脏等。其中，废弃食用油脂是指不可再食用的动植物油脂和油水混合物。" +
            "\n<color=#FFFFFF00>wdnm</color><b>投放要求：</b>厨余垃圾从产生时就应与其他品类垃圾分开，投放前要沥干水分，保证厨余垃圾分出质量，做到“无玻璃陶瓷、无金属杂物、无塑料橡胶”。纯流质的食物垃圾，如牛奶等，应直接倒进下水道。有包装物的过期食品应将包装物去除后分类投放，包装物则投放到对应的可回收物或者其他垃圾处理容器。",
        //可回收物
        "<color=#FFFFFF00>wdnm</color><b>可回收物：</b>" +
            "\n<color=#FFFFFF00>wdnm</color>是指在日常生活中或者为日常生活提供服务的活动中产生的，已经失去原有全部或部分使用价值，回收后经过再加工可以成为生产原料或者经过整理可以再利用的物品，主要包括废纸类、塑料类、玻璃类、金属类、电子废弃物类，织物类等。" +
            "\n<color=#FFFFFF00>wdnm</color><b>投放要求：</b>可回收物分类投放时应尽量保持清洁干燥，避免污染。废纸应保持平整；立体包装物应清空、清洁后压扁投放；玻璃制品应轻投轻放，有尖锐边角的应包裹后投放。",
        //有害垃圾
        "<color=#FFFFFF00>wdnm</color><b>有害垃圾：</b>" +
            "\n<color=#FFFFFF00>wdnm</color>是指生活垃圾中的有毒有害物质。包括废电池（铬镍电池、氧化汞电池、铅蓄电池等），废荧光灯管（日光灯管、节能灯等），废温度计，废血压计，杀虫剂及其包装物，过期药品及其包装物，废油漆、溶剂及其包装物等。" +
            "\n<color=#FFFFFF00>wdnm</color><b>投放要求：</b>有害垃圾投放应保证器物完整，避免二次污染。镍镉电池、氧化汞电池、铅蓄电池等，投放时应注意轻放；油漆桶、杀虫剂瓶子等，如有残留应密闭后投放；荧光灯、节能灯等易破损物品，应连带包装或包裹后投放；过期药物应连带包装一并投放。易挥发的有害垃圾，请密封后投放。其余物则投放到对应的可回收物或者其他垃圾处理容器。",
        //其他垃圾
        "<color=#FFFFFF00>wdnm</color><b>其他垃圾：</b>" +
            "\n<color=#FFFFFF00>wdnm</color>指除厨余垃圾、可回收物、有害垃圾之外的生活垃圾，以及难以辨识类别的生活垃圾。主要包括餐盒、餐巾纸、湿纸巾、卫生纸、塑料袋、食品包装袋、污染纸张、烟蒂、纸尿裤、一次性餐具、大骨头、贝壳、花盆、陶瓷碎片等。简而言之，难以辨识类别的生活垃圾可投入其他垃圾收集容器内。" +
            "\n<color=#FFFFFF00>wdnm</color><b>投放要求：</b>沥干水分后投放。"
    };

    private AudioSource audio_Btn;
    void Start()
    {
        mainPanel.SetActive(true);
        startPanel.SetActive(false);
        loadPanel.SetActive(false);

        audio_Btn = this.GetComponent<AudioSource>();

        Txt_Content.text = "";
        Txt_Content.fontSize =50;
    }
    /// <summary>
    /// 指导手册
    /// </summary>
    public void OnButtonClick0()
    {
        audio_Btn.Play();
        mainPanel.SetActive(false);
        startPanel.SetActive(true);
        Img_Video.SetActive(false);
        Txt_Content.text = "";
    }

    /// <summary>
    /// 场景教学
    /// </summary>
    public void OnButtonClick1()
    {
        audio_Btn.Play();
        if (!isLoadingScene)
        {
            isLoadingScene = true;
            loadPanel.SetActive(true);
            ChangeSceneIndex = 1;
            StartCoroutine(changeScene());
        }
    }

    /// <summary>
    /// 场景教学VR
    /// </summary>
    public void OnButtonClick2()
    {
        if (!isLoadingScene)
        {
            isLoadingScene = true;
            loadPanel.SetActive(true);
            ChangeSceneIndex = 2;
            StartCoroutine(changeScene());
        }
        audio_Btn.Play();
    }

    /// <summary>
    /// 家庭环境
    /// </summary>
    public void OnButtonClick3()
    {
        if (!isLoadingScene)
        {
            isLoadingScene = true;
            loadPanel.SetActive(true);
            ChangeSceneIndex = 3;
            StartCoroutine(changeScene());
        }
        audio_Btn.Play();
    }

    /// <summary>
    /// 家庭环境VR
    /// </summary>
    public void OnButtonClick4()
    {
        if (!isLoadingScene)
        {
            isLoadingScene = true;
            loadPanel.SetActive(true);
            ChangeSceneIndex = 4;
            StartCoroutine(changeScene());
        }
        audio_Btn.Play();
    }

    /// <summary>
    /// 垃圾处理的现状
    /// </summary>
    public void OnxianzhuangClick()
    {
        Txt_Content.text = txt[0];
        Txt_Content.fontSize = 50;
        Img_Video.SetActive(false);
        audio_Btn.Play();

    }

    /// <summary>
    /// 指导手册意义
    /// </summary>
    public void OnyiyiClick()
    {
        Txt_Content.text = txt[1];
        Txt_Content.fontSize = 44;
        Img_Video.SetActive(false);
        audio_Btn.Play();

    }

    /// <summary>
    /// 指导手册标准
    /// </summary>
    public void OnBiaozhunClick()
    {
        Txt_Content.text = txt[2];
        Txt_Content.fontSize = 50;
        Img_Video.SetActive(false);
        audio_Btn.Play();

    }

    /// <summary>
    /// 厨余垃圾
    /// </summary>
    public void OnchuyuClick()
    {
        Txt_Content.text = txt[3];
        Txt_Content.fontSize = 50;
        Img_Video.SetActive(false);
        audio_Btn.Play();

    }

    /// <summary>
    /// 可回收物
    /// </summary>
    public void OnkehuishouClick()
    {
        Txt_Content.text = txt[4];
        Txt_Content.fontSize = 50;
        Img_Video.SetActive(false);
        audio_Btn.Play();

    }

    /// <summary>
    /// 有害垃圾
    /// </summary>
    public void OnyouhaiClick()
    {
        Txt_Content.text = txt[5];
        Txt_Content.fontSize = 50;
        Img_Video.SetActive(false);
        audio_Btn.Play();

    }

    /// <summary>
    /// 其他垃圾
    /// </summary>
    public void OnqitaClick()
    {
        Txt_Content.text = txt[6];
        Txt_Content.fontSize = 50;
        Img_Video.SetActive(false);
        audio_Btn.Play();

    }

    /// <summary>
    /// 播放视频
    /// </summary>
    public void OnVideoClick()
    {
        Img_Video.SetActive(true);
        Img_Video.GetComponent<video>().PlayFromBegin();
        audio_Btn.Play();

    }

    public void ReturnMainPanel()
    {
        startPanel.SetActive(false);
        mainPanel.SetActive(true);
        audio_Btn.Play();
    }

    public void QuitScene()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    void Update()
    {
        if (operation == null)
        {
            return;
        }
        if (operation.progress < 0.899)
        {
            progress = operation.progress * 100;
        }
        else
        {
            progress = 100;
        }
        if (Curprogress < progress)
        {
            Curprogress += Random.Range(0,80) * 1.0f / 100;
            Curprogress = Mathf.Clamp(Curprogress, 0, 100);
        }
        slider.value = Curprogress;
        Txt_Progress.text = string.Format("{0:P}", Curprogress/100);

        if (Curprogress >= 100)
        {
            operation.allowSceneActivation = true;
        }

    }

    IEnumerator changeScene()
    {
        operation = SceneManager.LoadSceneAsync(ChangeSceneIndex, LoadSceneMode.Single);
        operation.allowSceneActivation = false;
        while (!operation.isDone&&operation.progress<1)
        {
            yield return operation;
        }
    }
}

/*17281232
朱昊
北京交通大学
计算机与信息技术学院
计科1704班
指导教师：王冰*/
