﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeManager : MonoBehaviour {

	public UIManager uiManager;
	public GameObject[] rubbish;
	private int count;
	private int precount = 0;
	// Use this for initialization
	void Start () {
		uiManager.ClearTextProgress();
	}
	
	// Update is called once per frame
	void Update () {
		count = 0;
		foreach (GameObject element in rubbish)
			if (element == null)
				count++;
        if (count != precount)
        {
			precount = count;
			uiManager.SetTextProgress("当前进度：" + count + "/" + rubbish.Length);
			Invoke("ClearProgress", 5);
        }
	}

	void ClearProgress()
    {
		uiManager.ClearTextProgress();
    }
}
